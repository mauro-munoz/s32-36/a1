const express = require('express');
const router = express.Router();
//const bcrypt = require('bcrypt');
const {verifyToken, decode, verifyAdmin} = require(`./../auth`)
// const ejs = require('ejs');


//import controllers
const controller = require(`./../controllers/controllers`)
//get user information

router.get(`/profile`,verifyToken,async (req,res)=>{
    // console.log(req.headers.authorization)
    // console.log(`welcome to get request`)
    const userId = decode(req.headers.authorization).id
    // console.log(userId)
    try{
        controller.userProfile(userId).then(result => res.send(result))
    }catch(err){
        res.status(500).json(err)
    }
})

//update a user
router.put('/updateUserProfile', verifyToken,async (req, res) => {
  const userId = (decode(req.headers.authorization).id)
    try{
     await controller.updateOneUser(userId,req.body).then((result,err) => {
      if(result){result.password = "***"
        return res.send(result)}
        else{return res.send(err)} })
    }catch(err){return res.status(500).send(err.message)}   
})

//update a user password
router.put('/update-password', verifyToken,async (req, res) => {

    const userId = (decode(req.headers.authorization).id)
      try{
       await controller.updatePassword(userId,req.body).then((result,err) => {
        if(result){result.password = "***"
          return res.send(result)}
          else{return res.send(err)} })
      }catch(err){return res.status(500).send(err.message)}   
  })

//create a user
router.post(`/new`, async (req,res)=>{
    
    try{
        await controller.createUser(req.body).then(result => res.send(result))
    }catch(err){res.send(`error po`)}
})




//get all users
router.get('/', async (req, res) => {
    try{
        await controller.getAllUsers().then(result=> res.send(result))
    }catch(err){
        return res.send(err.message)
    }
})

//get one user
router.get(`/:userId`,async(req,res)=>{
    try{
        await controller.getOneUser(req.params.userId).then(result => res.send(result))
    }catch(err){return res.send(err.message)}
})




//delete user
router.delete(`/delete`,verifyAdmin, async (req,res) => {
    try{
    await controller.deleteOneUser(req.body).then(result=>res.send (result))
    }catch(err){return res.send(err.message)}   
})

//login user
router.post(`/login`, async(req,res)=>{
    try{
        // res.send(`welcome to login`)
        await controller.loginUser(req.body).then(result=>res.send(result))
    }catch(err){return res.status(500).send(err.message)}
})

//admin access
router.put(`/isAdmin`,verifyToken,async (req, res)=>{
    const userId = (decode(req.headers.authorization).id)
    const isAdmin = (decode(req.headers.authorization).isAdmin)
    console.log(userId)
    // res.send(`${userId} ${isAdmin}`)
    if(isAdmin == true){
          try{
        await controller.adminAccess(req.body).then(result=>res.send(result))
    }catch(err){return res.status(500).send(err.message)}
    }else {res.send(`you are not authorized`)}
  
})



//export router to be imported in index.js
module.exports = router