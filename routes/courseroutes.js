const express = require('express');
const router = express.Router();
// import authentication
const {verifyToken, decode, verifyAdmin} = require(`./../auth`)

//import course controllers
const coursecontroller = require(`./../controllers/coursecontrollers`)

//add course
router.post(`/addCourse`,verifyAdmin,async (req,res)=>{
    try{
        await coursecontroller.addCourse(req.body).then(result=>res.send(result))
        }catch(err){return res.send(err.message)}   
})

//get all courses
router.get(`/`,verifyToken, async (req,res)=>{
    try{
        await coursecontroller.getCourses().then(result=>res.send(result))
    }catch(err){return res.send(err.message)}
})

//get specific course using id
router.get(`/:courseId`, verifyToken , async (req, res)=>{
    try{
        await coursecontroller.getOneCourse(req.params.courseId,req.body).then(result=>res.send(result))
    }catch(err){return res.send(err.message)}
})

//archive a course
router.put(`/archive/:courseId`,verifyAdmin, async (req, res)=>{
    try{
        await coursecontroller.archiveCourse(req.params.courseId, req.body).then(result=>res.send(result))
    }catch(err){return res.send(err.message)}
})

//get all active course
router.get(`/isActive`,verifyToken, async (req,res)=>{
    
    try{
        await coursecontroller.activeCourse(req.body).then(result=>res.send(result))
    }catch(err){return res.send(err.message)}
})

//delete a course
router.delete(`/delete/:courseId`,verifyAdmin, async (req, res)=>{
    try{
        await coursecontroller.deleteCourse(req.params.courseId).then(result=>res.send(result))
    }catch(err){return res.send(err.message)}
})

module.exports = router