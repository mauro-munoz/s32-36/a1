const jwt = require(`jsonwebtoken`)



module.exports.createToken = (payload) => {
    let userData = {
        id: payload._id,
        email:payload.email,
        isAdmin: payload.isAdmin
    }
    return  jwt.sign(userData,process.env.SECRET_PASS)
    
}

// module.exports.decryptToken = (payload) => {
    
//     return jwt.decrypt(payload,process.env.SECRET)
// }



//verify method

module.exports.verifyToken = (req,res,next) => {
        const requestToken = req.headers.authorization
        
        if(typeof requestToken == "undefined"){
            res.status(401).send(`Token Missing`)
        } else {
            const token = requestToken.slice(7,requestToken.length)
        
             if(typeof token !== "undefined"){
                return jwt.verify(token, process.env.SECRET_PASS,(err,data)=> {
                    if(err){
                        return res.send({auth: `auth failed`})
                    }else {
                        next()
                    }
                })
             }
        }
}

//verify admin method

module.exports.verifyAdmin = (req,res,next) => {
    const requestToken = req.headers.authorization
    
    if(typeof requestToken == "undefined"){
        res.status(401).send(`Token Missing`)
    } else {
        const token = requestToken.slice(7,requestToken.length)
    
         if(typeof token !== "undefined"){

            const admin = jwt.decode(token).isAdmin
            if(admin){
                return jwt.verify(token, process.env.SECRET_PASS,(err,data)=> {
                    if(err){
                        return res.send({auth: `auth failed`})
                    }else {
                        next()
                    }
                })
            }else{
                res.status(403).send(`you are not authorized`)
            }
         }
    }
}


//decode
module.exports.decode = (bearerToken) => {

    const token = bearerToken.slice(7,bearerToken.length)
    return jwt.decode(token)
    // console.log(bearerToken)
}