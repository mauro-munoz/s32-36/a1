const mongoose = require('mongoose');

const courseSchema = new mongoose.Schema({
    courseName: {
        type: String,
        required: [true, `Course name is required`],
        unique: true
    },
    description: {
        type: String,
        required: [true, `Course description is required`]
    },
    price: {
        type: Number,
        required: [true, `Price is required`]
    },
    isOffered: {
        type: Boolean,
        default: true
    },
    enrollees: [
        {
            userId: {
                type: String,
                required: [true, `userId is required`]
            },
            enrolledOn: {
                type: Date,
                default: new Date()
            }
        }
    ]
}, {timestamps: true})

module.exports = mongoose.model("Course", courseSchema);