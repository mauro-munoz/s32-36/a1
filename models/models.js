const mongoose = require('mongoose')

const userSchema = new mongoose.Schema({
    "name" :{
        type: String,
        required: [true,`Name is required`]
       
    },
    "email":{
        type: String,
        required: [true,`Email is required`],
        unique: true
    },
    "password": {
        type: String,
        required: [true,`Password is required`]
       
    },
    "isAdmin":{
        type: Boolean,
        default: false
    },
    "enrolledCourses": [
        {
            "courseId": {
                type: String,
                required: [true,`CourseId is required`]
            },
            "status": {
                type: String,
                default: "Enrolled",
            },
            enrolledOn: {
                type: Date,
                default: new Date()
            }
        }
    ]
},{ timestamps: true})



module.exports = mongoose.model('userModel',userSchema)