const myUser = require('./../models/models')
//const bcrypt = require('bcrypt');
const CryptoJS = require('crypto-js');

const {createToken} = require('./../auth');

//create new users
module.exports.createUser = async (reqBody) => {
    // console.log(`mauro`)
    return await myUser.findOne({email: reqBody.email}).then((result)=>{
                    if(result != null && result.email== reqBody.email) {
                        return false
                    }else{
                            let newUser = new myUser({
                                "name" : reqBody.name,
                                "email" : reqBody.email,
                                "password" : 
                                // reqBody.password,
                                CryptoJS.AES.encrypt(reqBody.password, process.env.SECRET_PASS).toString(),
                                "isAdmin" : reqBody.isAdmin
                            })

                             return newUser.save().then(()=>
                             {return true}
                             ).catch(`error`)

                    }
            })
           
}

//get all users
module.exports.getAllUsers = async () => {
    return await myUser.find({}).then(result=>result)
}

//get one user with userId
module.exports.getOneUser = async (id) => {
    return await myUser.findById(id).then(result=>result)
}

//update one user using userId
module.exports.updateOneUser = async (id,reqBody) => {
    const userData = {
        name : reqBody.name,
        email: reqBody.email,
        password : CryptoJS.AES.encrypt(reqBody.password, process.env.SECRET_PASS).toString(),
        enrolledCourses : reqBody.enrolledCourses
    }
    return await myUser.findByIdAndUpdate(id,{$set:userData},{new:true}).then(result=>result).catch(err=>err)
}

//delete one user
module.exports.deleteOneUser = async (reqBody) => {
    return await myUser.findOneAndDelete({email:reqBody.email}).then(result => `${result.name} was deleted`)
}

//login user
module.exports.loginUser = async(reqBody) => {
    return await myUser.findOne({email: reqBody.email}).then(result=>{
        if(result == null){
            return {message:"User does not exist"}
        }else{
            if(result !== null){
              let decrypted =  CryptoJS.AES.decrypt(result.password, process.env.SECRET_PASS).toString(CryptoJS.enc.Utf8)
                // console.log(reqBody.password)

                if(reqBody.password == decrypted){
                   return {token: createToken(result)}
                } else {
                    return {auth: `auth failed ${decrypted}`}
                }
            }else {
                return err}
        }
    })
}

//user profile
module.exports.userProfile = async (id) => {
    return await myUser.findById(id).then(result=>result).catch(err=>{err})
}

//update user password
module.exports.updatePassword = async (id,reqBody) => {

    const userData = {
        password : CryptoJS.AES.encrypt(reqBody.password, process.env.SECRET_PASS).toString(),
    }
    return await myUser.findByIdAndUpdate(id,{$set:userData},{new:true}).then(result=>result).catch(err=>err)
}

module.exports.adminAccess = async(reqBody) => {
    const userAccess = {
        isAdmin: reqBody.isAdmin
    }
    return await myUser.findOneAndUpdate({email:reqBody.email},{$set:userAccess},{new:true}).then(result=>result).catch(err=>err)
}