const myCourses = require('./../models/coursemodels')


//get all active courses
module.exports.activeCourse =async (reqBody) => {
    return await myCourses.find({isOffered: reqBody.isOffered}).then(result => result).catch(err => err)
}

//add course
module.exports.addCourse = async (reqBody) => {
    return await myCourses.findOne({courseName: reqBody.courseName}).then((result)=>{
        if(result != null && result.courseName== reqBody.courseName) {
            return `Course is already registered`
        }else{
                let newCourse = new myCourses({
                    "courseName" : reqBody.courseName,
                    "description" : reqBody.description,
                    "price" : reqBody.price,
                    "isOffered" : true,
                    "enrollees" : reqBody.enrollees
                })

                 return newCourse.save().then(()=>
                 {return true}
                 ).catch(`error`)

        }
})
}

//get all courses

module.exports.getCourses = async () => {
   return await myCourses.find({}).then(result => result).catch(err => err)
}

//get one course

module.exports.getOneCourse = async (id,reqBody) => {
    return await myCourses.findOne({courseName: reqBody.courseName}).then(result => result).catch(err => err)
}

//archive course
module.exports.archiveCourse = async (id,reqBody) => {
    return await myCourses.findByIdAndUpdate(id, {$set: reqBody},{new: true}).then(result => result).catch(err => err)
}

// delete course
module.exports.deleteCourse = async (id) => {
    return await myCourses.findByIdAndDelete(id).then(result => {return `${result.courseName} has been deleted`}).catch(err => err)
}